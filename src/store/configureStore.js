import { createStore, combineReducers } from 'redux';
import placesReducer from './reducers/places';

const reducer = combineReducers ({  places:placesReducer });

const store = createStore(reducer);

export default store;