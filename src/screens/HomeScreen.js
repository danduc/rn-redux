import React from 'react';
import { View, Text, Button } from 'react-native';

class HomeScreen extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text>Home Screen</Text>
          <Button
            title="Go to Redux demo"
            onPress={() => {
              this.props.navigation.navigate('Places') 
            }}
          />
        </View>
      );
    }  
  }

  export default HomeScreen;