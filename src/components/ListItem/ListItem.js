import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';

const ListItem = ({ placeName, placeImage, onItemPressed })=> (
    <TouchableOpacity onPress={  onItemPressed }>
        <View style={ styles.listItem } >
            <Image resizeMode="cover" source={ placeImage } style={ styles.placeImage } />
            <Text>{ placeName }</Text>
        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    listItem:{
        width:"100%",
        padding:10,
        backgroundColor:"#eee",
        marginBottom:5,
        flexDirection:"row",
        alignItems:"center"
    },
    placeImage:{
        marginRight:8,
        height:30,
        width:30
    }
});

export default ListItem;