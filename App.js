import React from 'react';
import { Provider } from 'react-redux';
import Router from './Router';
import configureStore from './src/store/configureStore';

class App extends React.Component {

  render() {

    const store = configureStore;
    
    return (
      <Provider store={ store }>
        <Router />
      </Provider>
    )
  }
}

export default App;
