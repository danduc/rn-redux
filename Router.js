import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './src/screens/HomeScreen';
import PlacesScreen from './src/screens/PlacesScreen';

const AppNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen,
  },
  Places: {
    screen: PlacesScreen,
  },
}, {
    initialRouteName: 'Home',
});

export default createAppContainer(AppNavigator)